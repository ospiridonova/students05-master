package main.services;

import main.models.dao.UserDao;
import main.models.pojo.User;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

/**
 * Created by admin on 20.04.2017.
 */
@Service
public class UserServiceImpl implements UserService {
    private static final Logger logger = Logger.getLogger(UserServiceImpl.class);
    private UserDao userDao;

    @Autowired
    public UserServiceImpl(UserDao userDao) {
        this.userDao = userDao;
    }

    public User auth(String login, String password) {
        User user = userDao.findUserByLoginAndPassword(login, password);
        logger.debug("user: " + user);

        if (user != null && user.isIs_block()) {
            return null;
        }
        logger.debug("user not blocked");

        return user;
    }
}
